package net.riotopsys.demo.weather.inject;

import net.riotopsys.demo.weather.task.WeatherTasks;
import net.riotopsys.demo.weather.view.DetailActivity;
import net.riotopsys.demo.weather.view.ListResultFragment;
import net.riotopsys.demo.weather.view.MainActivity;
import net.riotopsys.demo.weather.view_model.ListItemViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by adam on 7/31/16.
 */
@Singleton
@Component(
        modules = {
                MainModule.class
        }
)
public interface MainComponent {

        void inject(WeatherTasks task);

        void inject(ListResultFragment listResultFragment);

        ListItemViewModel create();

        void inject(MainActivity mainActivity);

        void inject(DetailActivity detailActivity);
}
