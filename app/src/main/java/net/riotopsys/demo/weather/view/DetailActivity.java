package net.riotopsys.demo.weather.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import net.riotopsys.demo.weather.databinding.DetailActivityBinding;
import net.riotopsys.demo.weather.inject.InjectedApplication;
import net.riotopsys.demo.weather.view_model.DetailViewModel;
import net.riotopsys.demo.weather.view_model.ListItemViewModel;

import javax.inject.Inject;


/**
 * Created by adam.fitzgerald on 8/18/16.
 */
public class DetailActivity extends AppCompatActivity {
    public static final String RECORD = DetailActivity.class.getCanonicalName()+"+record";

    private String data;
    private DetailActivityBinding binding;

    @Inject
    protected DetailViewModel dvm;

    public DetailActivity() {
        InjectedApplication.getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DetailActivityBinding.inflate(LayoutInflater.from(this));
        setContentView(binding.getRoot());

        binding.setDvm(dvm);


        data = getIntent().getStringExtra(RECORD);
        dvm.setData(data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        dvm.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dvm.onResume();
    }
}
