package net.riotopsys.demo.weather.view_model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import net.riotopsys.demo.weather.R;
import net.riotopsys.demo.weather.network.WeatherModel;
import net.riotopsys.demo.weather.task.GetWeatherRequest;
import net.riotopsys.factotum.api.AbstractRequest;
import net.riotopsys.factotum.api.Factotum;
import net.riotopsys.factotum.api.SimpleCancelRequest;
import net.riotopsys.factotum.api.interfaces.ICallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import hugo.weaving.DebugLog;

/**
 * Created by adam.fitzgerald on 8/18/16.
 */
public class DetailViewModel extends BaseObservable implements ICallback<WeatherModel> {

    private String data = null;
    private Factotum factotum;
    private Context context;

    private WeatherModel weatherModel = null;
    private SimpleDateFormat sdf;
    private String placeholder;

    @Inject
    public DetailViewModel(Factotum factotum, Context context ) {
        this.factotum = factotum;
        this.context = context;
        sdf = new SimpleDateFormat(context.getString(R.string.date_format), Locale.getDefault());
        placeholder = context.getString(R.string.placeholder);
    }


    public void setData(String data) {
        this.data = data;
    }

    @DebugLog
    @Override
    public void onSuccess(AbstractRequest request, WeatherModel result) {
        weatherModel = result;
        notifyChange();
    }

    @Override
    public void onFailure(AbstractRequest request, Object error) {
        weatherModel = null;
        notifyChange();
        Toast.makeText(context,"Unable to load weather data", Toast.LENGTH_LONG).show();
    }

    public void onPause() {
        factotum.issueCancellation(new SimpleCancelRequest(this));
    }

    public void onResume() {
        factotum.addRequest( new GetWeatherRequest(data)
                .setGroup(this)
                .setCallback(this));
    }

    private String format( Float value ){
        if ( value == null ){
            return context.getString(R.string.placeholder);
        }
        return String.format(Locale.getDefault(), context.getString(R.string.float_template), value);
    }

    private String format(Date value) {
        if ( value == null ){
            return placeholder;
        }
        return sdf.format(value);
    }

    @Bindable
    public String getZipCode(){
        return context.getString(R.string.zip_label) + data;
    }

    @Bindable
    public String getApparentTemperature() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.temp_label) + format( weatherModel.currently.apparentTemperature) ;
    }

    @Bindable
    public String getCloudCover() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.cloud_cover_label) + format( weatherModel.currently.cloudCover) ;
    }

    @Bindable
    public String getDewPoint() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.dew_point_label) + format( weatherModel.currently.dewPoint) ;
    }

    @Bindable
    public String getHumidity() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.humidity_label) + format( weatherModel.currently.humidity) ;
    }

    @Bindable
    public String getNearestStormBearing() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.storm_bearing_label) + format( weatherModel.currently.nearestStormBearing) ;
    }

    @Bindable
    public String getNearestStormDistance() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.storm_dis_label)+ format( weatherModel.currently.nearestStormDistance) ;
    }

    @Bindable
    public String getOzone() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.ozone_label) + format( weatherModel.currently.ozone) ;
    }

    @Bindable
    public String getPrecipIntensity() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.precip_intencity_label) + format( weatherModel.currently.precipIntensity) ;
    }

    @Bindable
    public String getPrecipProbability() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.precip_chance_label) + format( weatherModel.currently.precipProbability) ;
    }

    @Bindable
    public String getPressure() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.pressure_label) + format( weatherModel.currently.pressure) ;
    }

    @Bindable
    public String getSummary() {
        if ( weatherModel == null ) return placeholder;
        return  context.getString(R.string.summary_label) + weatherModel.currently.summary;
    }

    @Bindable
    public String getTemperature() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.temperature_label) + format( weatherModel.currently.temperature) ;
    }

    @Bindable
    public String getTime() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.time_label) + format( weatherModel.currently.time ) ;
    }

    @Bindable
    public String getUvIndex() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.uv_label) + format( weatherModel.currently.uvIndex) ;
    }

    @Bindable
    public String getVisibility() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.vis_label) + format( weatherModel.currently.visibility) ;
    }

    @Bindable
    public String getWindBearing() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.wind_bearing_label) + format( weatherModel.currently.windBearing) ;
    }

    @Bindable
    public String getWindGust() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.wind_gust_label) + format( weatherModel.currently.windGust) ;
    }

    @Bindable
    public String getWindSpeed() {
        if ( weatherModel == null ) return placeholder;
        return context.getString(R.string.wind_speed_label) + format( weatherModel.currently.windSpeed) ;
    }

}
