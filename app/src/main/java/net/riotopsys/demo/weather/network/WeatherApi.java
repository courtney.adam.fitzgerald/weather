package net.riotopsys.demo.weather.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by adam on 10/28/17.
 */

public interface WeatherApi {

//    https://api.darksky.net/forecast/0123456789abcdef9876543210fedcba/42.3601,-71.0589
    @GET("forecast/{key}/{lat},{lon}")
    Call<WeatherModel> getPlaceModel(@Path("key") String key, @Path("lat") double lat, @Path("lon") double lon);

}
