package net.riotopsys.demo.weather.task;

import android.location.Address;
import android.location.Geocoder;

import net.riotopsys.demo.weather.BuildConfig;
import net.riotopsys.demo.weather.network.WeatherModel;
import net.riotopsys.demo.weather.network.WeatherApi;
import net.riotopsys.factotum.api.annotation.Task;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import retrofit2.Response;

/**
 * Created by adam on 7/31/16.
 */
public class WeatherTasks {

    @Inject
    public WeatherApi weatherApi;

    @Inject
    public Geocoder geocoder;

    @DebugLog
    @Task
    public WeatherModel getWeather(String zipCode) throws IOException {

        List<Address> addresses = geocoder.getFromLocationName(zipCode, 1);

        if (!addresses.isEmpty()) {
            Address address = addresses.get(0);

            Response<WeatherModel> response = weatherApi.getPlaceModel(BuildConfig.API_KEY, address.getLatitude(), address.getLongitude()).execute();

            if (response.isSuccessful()) {
                return response.body();
            } else {
                throw new RuntimeException("give real message later");
            }

        }
        throw new RuntimeException("give real message later");

    }
}
