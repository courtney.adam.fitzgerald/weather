package net.riotopsys.demo.weather.data;

import android.os.Handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Created by adam.fitzgerald on 8/16/16.
 */
public class WatchableList<T> extends ArrayList<T> {

    private Handler handler;

    public WatchableList(Handler handler) {
        this.handler = handler;
    }

    private Set<Subscription<T>> subscriptions = Collections.newSetFromMap(new WeakHashMap<Subscription<T>, Boolean>());

    public interface Subscription<T> {
        void updated(WatchableList<T> list);
    }

    public void register(Subscription<T> subscription) {
        subscriptions.add(subscription);
    }

    private void sendNotice() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (Subscription<T> subscription : subscriptions) {
                    subscription.updated(WatchableList.this);
                }
            }
        });
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        try {
            return super.removeAll(collection);
        } finally {
            sendNotice();
        }
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        try {
            return super.retainAll(collection);
        } finally {
            sendNotice();
        }
    }

    @Override
    public T remove(int index) {
        try {
            return super.remove(index);
        } finally {
            sendNotice();
        }
    }

    @Override
    public boolean remove(Object object) {
        try {
            return super.remove(object);
        } finally {
            sendNotice();
        }
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        try {
            super.removeRange(fromIndex, toIndex);
        } finally {
            sendNotice();
        }
    }

    @Override
    public T set(int index, T object) {
        try {
            return super.set(index, object);
        } finally {
            sendNotice();
        }
    }

    @Override
    public boolean add(T object) {
        try {
            return super.add(object);
        } finally {
            sendNotice();
        }
    }

    @Override
    public void add(int index, T object) {
        try {
            super.add(index, object);
        } finally {
            sendNotice();
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        try {
            return super.addAll(collection);
        } finally {
            sendNotice();
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> collection) {
        try {
            return super.addAll(index, collection);
        } finally {
            sendNotice();
        }
    }

    @Override
    public void clear() {
        try {
            super.clear();
        } finally {
            sendNotice();
        }
    }
}
