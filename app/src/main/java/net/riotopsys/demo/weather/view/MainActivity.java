package net.riotopsys.demo.weather.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import net.riotopsys.demo.weather.R;
import net.riotopsys.demo.weather.inject.InjectedApplication;

import hugo.weaving.DebugLog;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 0xC;
    private static final String TAG = MainActivity.class.getSimpleName();

    private ListResultFragment listResultFragment = null;

    public MainActivity() {
        InjectedApplication.getComponent().inject(this);
    }

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        listResultFragment = (ListResultFragment) fragmentManager.findFragmentById(R.id.frame);

        if ( listResultFragment == null ) {
            listResultFragment = new ListResultFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.frame, listResultFragment)
                    .commit();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }



}
