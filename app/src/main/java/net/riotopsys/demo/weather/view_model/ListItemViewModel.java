package net.riotopsys.demo.weather.view_model;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AlertDialog;
import android.view.View;

import net.riotopsys.demo.weather.R;
import net.riotopsys.demo.weather.data.WatchableList;
import net.riotopsys.demo.weather.inject.Named;
import net.riotopsys.demo.weather.view.DetailActivity;

import javax.inject.Inject;

import static net.riotopsys.demo.weather.inject.Names.PLACES;

/**
 * Created by adam.fitzgerald on 8/17/16.
 */
public class ListItemViewModel extends BaseObservable implements View.OnClickListener, View.OnLongClickListener{


    private WatchableList<String> list;
    private Context context;

    @Inject
    ListItemViewModel(@Named(PLACES) WatchableList<String> list, Context context){
        this.list = list;
        this.context = context;
    }

    String data;

    public void setData(String data) {
        this.data = data;
        notifyChange();
    }

    @Bindable
    public String getZip(){
        return context.getString(R.string.zip_label)+ data;
    }

    @Override
    public void onClick(View view) {
        view.getContext().startActivity(new Intent( view.getContext(), DetailActivity.class).putExtra(DetailActivity.RECORD,data));
    }

    @Override
    public boolean onLongClick(View view) {

        Context ctx = view.getContext();
        new AlertDialog.Builder(ctx)
                .setTitle(R.string.confirm_prompt_title)
                .setMessage(ctx.getString(R.string.confirm_prompt_body_template, data))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        list.remove(data);
                    }
                })
                .create()
                .show();

        return true;
    }
}
