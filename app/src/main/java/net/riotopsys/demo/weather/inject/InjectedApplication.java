package net.riotopsys.demo.weather.inject;

import android.app.Application;

/**
 * Created by adam on 7/31/16.
 */
public class InjectedApplication extends Application {

    private static MainComponent component = null;

    public static MainComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .build();
    }
}
