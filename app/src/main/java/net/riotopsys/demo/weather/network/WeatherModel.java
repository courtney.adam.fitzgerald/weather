package net.riotopsys.demo.weather.network;

import java.util.Date;

/**
 * Created by adam on 10/28/17.
 */

public class WeatherModel {

    public CurrentWeather currently;

    public static class CurrentWeather {
        public Float apparentTemperature;
        public Float cloudCover;
        public Float dewPoint;
        public Float humidity;
        public Float nearestStormBearing;
        public Float nearestStormDistance;
        public Float ozone;
        public Float precipIntensity;
        public Float precipProbability;
        public Float pressure;
        public String summary;
        public Float temperature;
        public Date time;
        public Float uvIndex;
        public Float visibility;
        public Float windBearing;
        public Float windGust;
        public Float windSpeed;

        @Override
        public String toString() {
            return "CurrentWeather{" +
                    "apparentTemperature=" + apparentTemperature +
                    ", cloudCover=" + cloudCover +
                    ", dewPoint=" + dewPoint +
                    ", humidity=" + humidity +
                    ", nearestStormBearing=" + nearestStormBearing +
                    ", nearestStormDistance=" + nearestStormDistance +
                    ", ozone=" + ozone +
                    ", precipIntensity=" + precipIntensity +
                    ", precipProbability=" + precipProbability +
                    ", pressure=" + pressure +
                    ", summary='" + summary + '\'' +
                    ", temperature=" + temperature +
                    ", time=" + time +
                    ", uvIndex=" + uvIndex +
                    ", visibility=" + visibility +
                    ", windBearing=" + windBearing +
                    ", windGust=" + windGust +
                    ", windSpeed=" + windSpeed +
                    '}';
        }

    }

    @Override
    public String toString() {
        return "WeatherModel{" +
                "currently=" + currently +
                '}';
    }
}
