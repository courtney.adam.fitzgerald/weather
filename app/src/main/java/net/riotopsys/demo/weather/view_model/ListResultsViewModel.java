package net.riotopsys.demo.weather.view_model;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import net.riotopsys.demo.weather.R;
import net.riotopsys.demo.weather.data.WatchableList;
import net.riotopsys.demo.weather.inject.Named;
import net.riotopsys.demo.weather.view.DetailActivity;
import net.riotopsys.demo.weather.view.ResultAdapter;
import net.riotopsys.demo.weather.widget.VerticalSpaceItemDecoration;

import java.util.regex.Pattern;

import javax.inject.Inject;

import hugo.weaving.DebugLog;

import static net.riotopsys.demo.weather.inject.Names.PLACES;

/**
 * Created by adam.fitzgerald on 8/16/16.
 */
@BindingMethods({
        @BindingMethod(type = RecyclerView.class,
                attribute = "adapter",
                method = "setAdapter"),
        @BindingMethod(type = RecyclerView.class,
                attribute = "decorator",
                method = "addItemDecoration"),
})
public class ListResultsViewModel extends BaseObservable implements View.OnClickListener{

    private WatchableList<String> list;

    private Pattern zipPattern = Pattern.compile("^[0-9]{5}(-[0-9]{4})?$");

    @Inject
    public ListResultsViewModel(@Named(PLACES) WatchableList<String> list) {
        this.list = list;
    }

    public RecyclerView.Adapter getAdapter() {
        return new ResultAdapter(list);
    }

    public RecyclerView.ItemDecoration decorator(){
        return new VerticalSpaceItemDecoration(30);
    }

    @DebugLog
    @Override
    public void onClick(View view) {
        final Context ctx = view.getContext();

        final EditText input = new EditText(ctx);

        new AlertDialog.Builder(ctx)
                .setView(input)
                .setTitle(R.string.add_zip)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String clean = input.getText().toString().trim();
                        if ( !list.contains(clean) ){
                            if ( zipPattern.matcher(clean).matches() ) {
                                list.add(clean);
                                ctx.startActivity(new Intent( ctx, DetailActivity.class).putExtra(DetailActivity.RECORD,clean));
                            } else {
                                new AlertDialog.Builder(ctx)
                                        .setMessage(ctx.getString(R.string.zip_error_template, clean))
                                        .create()
                                        .show();
                            }
                        }
                    }
                })
                .create()
                .show();
    }
}
