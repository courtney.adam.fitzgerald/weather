package net.riotopsys.demo.weather.inject;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by adam.fitzgerald on 8/16/16.
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface Named {
    String value();
}