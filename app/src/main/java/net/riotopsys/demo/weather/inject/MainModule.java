package net.riotopsys.demo.weather.inject;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import net.riotopsys.demo.weather.data.WatchableList;
import net.riotopsys.demo.weather.network.WeatherApi;
import net.riotopsys.factotum.api.Factotum;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hugo.weaving.DebugLog;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static net.riotopsys.demo.weather.inject.Names.PLACES;

/**
 * Created by adam on 7/31/16.
 */
@Module
public class MainModule {
    private static final String ZIP_KEY = "zips";

    private Context context;

    public MainModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext(){
        return context;
    }

    @Provides
    public Geocoder provideGeocoder(Context ctx){
        return new Geocoder(ctx, Locale.getDefault());
    }

    @Singleton
    @Provides
    public Factotum provideFactotum(){
        return new Factotum.Builder()
                .setOnTaskCreationCallback(new DaggerTaskInjector())
                .build();
    }

    @Provides
    public Retrofit provideRetrofitBuilder(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl("https://api.darksky.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    public Gson provideGson(){
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new TypeAdapter<Date>() {
                    @Override
                    public void write(JsonWriter out, Date value) throws IOException {
                        out.value(value.getTime());
                    }

                    @Override
                    public Date read(JsonReader in) throws IOException {
                        return new Date(in.nextLong());
                    }
                })
                .create();
    }


    @Singleton
    @Provides
    public OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.i("LOG", message);
                    }
                }).setLevel(HttpLoggingInterceptor.Level.BODY))
                .cache( new Cache(new File( Environment.getDataDirectory(), "restCache"), 10 * 1024 * 1024))
                .build();
    }

    @Provides
    public WeatherApi provideWeatherAPI(Retrofit retrofit){
        return retrofit.create(WeatherApi.class);
    }

    @DebugLog
    @Singleton
    @Provides
    @Named(PLACES)
    public WatchableList<String> providePlaces(Handler handler, Context context){

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        WatchableList<String> list = new WatchableList<>(handler);

        list.register(new WatchableList.Subscription<String>() {
            @DebugLog
            @Override
            public void updated(WatchableList<String> list) {
                prefs.edit().putStringSet(ZIP_KEY, new HashSet<>(list)).apply();
            }
        });

        list.addAll(prefs.getStringSet(ZIP_KEY, new HashSet<>(Arrays.asList("14024", "14624", "78664"))));

        return list;
    }

    @Provides
    public Handler provideHandler(){
        return new Handler(Looper.getMainLooper());
    }

}
