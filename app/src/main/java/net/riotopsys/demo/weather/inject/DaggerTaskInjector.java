package net.riotopsys.demo.weather.inject;

import net.riotopsys.demo.weather.task.WeatherTasks;
import net.riotopsys.factotum.api.interfaces.IOnTaskCreationCallback;

/**
 * Created by adam on 7/31/16.
 */
public class DaggerTaskInjector implements IOnTaskCreationCallback {

    @Override
    public void onTaskCreation(Object task) {
        if ( task instanceof WeatherTasks) {//worked so much better on OG dagger. :(
            InjectedApplication.getComponent().inject((WeatherTasks)task);
        }
    }
}
