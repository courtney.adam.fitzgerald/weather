package net.riotopsys.demo.weather.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.riotopsys.demo.weather.data.WatchableList;
import net.riotopsys.demo.weather.databinding.ListItemBinding;
import net.riotopsys.demo.weather.inject.InjectedApplication;
import net.riotopsys.demo.weather.view_model.ListItemViewModel;

/**
 * Created by adam.fitzgerald on 8/16/16.
 */
public class ResultAdapter extends RecyclerView.Adapter implements WatchableList.Subscription<String> {
    private WatchableList<String> list;

    private static class ViewHolder extends RecyclerView.ViewHolder{

        public final ListItemBinding binding;

        public ViewHolder(ListItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
            ListItemViewModel vm = InjectedApplication.getComponent().create();
            binding.setVm(vm);
        }
    }

    public ResultAdapter(WatchableList<String> list) {
        this.list = list;
        list.register(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.getContext()),  parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder)holder;
        vh.binding.getVm().setData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void updated(WatchableList<String> list) {
        notifyDataSetChanged();
    }

}
