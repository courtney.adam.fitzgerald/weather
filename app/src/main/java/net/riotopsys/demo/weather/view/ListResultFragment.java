package net.riotopsys.demo.weather.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.riotopsys.demo.weather.R;
import net.riotopsys.demo.weather.databinding.FragmentListBinding;
import net.riotopsys.demo.weather.inject.InjectedApplication;
import net.riotopsys.demo.weather.view_model.ListResultsViewModel;

import javax.inject.Inject;

import hugo.weaving.DebugLog;


/**
 * Created by adam.fitzgerald on 8/16/16.
 */
public class ListResultFragment extends Fragment {

    @Inject
    protected ListResultsViewModel vm;

    private FragmentListBinding binding;

    public ListResultFragment(){
        InjectedApplication.getComponent().inject(this);
    }

    @DebugLog
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        binding.setVm(vm);
        return binding.getRoot();
    }
}
